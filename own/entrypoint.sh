#!/bin/bash

if [ ! -e "/etc/nginx/sites-available/platform.conf" ]; then

  cp /etc/nginx/sites-available/default-platform.conf /etc/nginx/sites-available/platform.conf

  export DOMAIN=${DOMAIN}
  export UPSTREAM=${UPSTREAM}

  sed -i "s/website.tld/${DOMAIN}/" /etc/nginx/sites-available/platform.conf
  sed -i "s/localhost/${UPSTREAM}/" /etc/nginx/sites-available/platform.conf

  ln -s /etc/nginx/sites-available/platform.conf /etc/nginx/sites-enabled/platform.conf

  touch /var/log/nginx/${DOMAIN}-error.log

  rm /etc/nginx/sites-available/default-platform.conf

  echo "Nginx has just been configured"

else
  echo "Nginx is already configured"
fi

exec "$@"