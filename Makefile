REGISTRY=registry.gitlab.com
USERNAME=supernami
TAG=4

login:
	docker login -u ${USERNAME} ${REGISTRY}

new-tag: build tag push

build: hub own

tag: hub-tag own-tag

push: hub-push own-push


hub: hub-build hub-tag hub-push

hub-build:
	docker build -t ${REGISTRY}/${USERNAME}/affiliatelk/platform/nginx-platform-config/hub:${TAG} -f hub/hub-nginx.df .

hub-tag:
	docker tag ${REGISTRY}/${USERNAME}/affiliatelk/platform/nginx-platform-config/hub:${TAG} ${REGISTRY}/${USERNAME}/affiliatelk/platform/nginx-platform-config/hub:latest

hub-push:
	docker push ${REGISTRY}/${USERNAME}/affiliatelk/platform/nginx-platform-config/hub:${TAG} && \
	docker push ${REGISTRY}/${USERNAME}/affiliatelk/platform/nginx-platform-config/hub:latest

hub-pull:
	docker pull ${REGISTRY}/${USERNAME}/affiliatelk/platform/nginx-platform-config/hub:latest

hub-run:
	docker run -it -e DOMAIN=${PLATFORM_DOMAIN} -e UPSTREAM=${PLATFORM_UPSTREAM} ${REGISTRY}/affiliatelk/platform/nginx-platform-config/hub:latest bash


own: own-build own-tag own-push

own-build:
	docker build -t ${REGISTRY}/${USERNAME}/affiliatelk/platform/nginx-platform-config/own:${TAG} -f own/npcg.df .

own-tag:
	docker tag ${REGISTRY}/${USERNAME}/affiliatelk/platform/nginx-platform-config/own/npcg:${TAG} ${REGISTRY}/${USERNAME}/affiliatelk/platform/nginx-platform-config/own/npcg:latest

own-push:
	docker push ${REGISTRY}/${USERNAME}/affiliatelk/platform/nginx-platform-config/own/npcg:${TAG} && \
	docker push ${REGISTRY}/${USERNAME}/affiliatelk/platform/nginx-platform-config/own/npcg:latest

own-pull:
	docker pull ${REGISTRY}/${USERNAME}/affiliatelk/platform/nginx-platform-config/own/npcg:latest

own-run:
	docker run -it -e PLATFORM_DOMAIN=${PLATFORM_DOMAIN} -e PLATFORM_UPSTREAM=${PLATFORM_UPSTREAM} ${REGISTRY}/${USERNAME}/affiliatelk/platform/nginx-platform-config/own/npcg:latest bash